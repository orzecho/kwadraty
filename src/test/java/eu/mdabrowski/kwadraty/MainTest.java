package eu.mdabrowski.kwadraty;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MainTest {
    @Test
    public void checkAllSquares_should_return_positive() {
        int[][] tab = {{1, 1, 1, 0, 1}, {1, 1, 0, 0, 1}, {0, 0, 0, 0, 1}, {0, 1, 1, 0, 1}, {0, 1, 1, 0, 1}};

        Main main = new Main();
        assertTrue(main.checkAllSquares(tab));
    }

    @Test
    public void checkAllSquares_should_return_negative() {
        int[][] tab = {{0, 1, 0, 1, 0}, {1, 0, 1, 0, 1}, {0, 1, 0, 1, 0}, {1, 0, 1, 0, 1}, {0, 1, 0, 1, 0}};

        Main main = new Main();
        assertFalse(main.checkAllSquares(tab));
    }

    @Test
    public void checkSquare_should_return_negative() {
        int[][] tab = {{0, 1, 1, 0, 1}, {0, 1, 1, 0, 1}, {0, 1, 1, 0, 1}, {0, 1, 1, 0, 1}, {0, 1, 1, 0, 1}};

        Main main = new Main();
        assertFalse(main.checkSquare(tab, new SquareSlice(0, 0, 3)));
    }

    @Test
    public void checkSquare_should_return_positive() {
        int[][] tab = {{1, 1, 1, 0, 1}, {1, 1, 0, 0, 1}, {0, 0, 0, 0, 1}, {0, 1, 1, 0, 1}, {0, 1, 1, 0, 1}};

        Main main = new Main();
        assertTrue(main.checkSquare(tab, new SquareSlice(0, 0, 3)));
    }
}
