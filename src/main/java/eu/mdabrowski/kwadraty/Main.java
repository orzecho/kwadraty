package eu.mdabrowski.kwadraty;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public boolean checkAllSquares(int[][] tab) {
        List<SquareSlice> squareSlices = new ArrayList<>();
        for (int x = 0; x < tab.length - 3; x++) {
            for (int y = 0; y < tab.length - 3; y++) {
                for (int size = 3; Math.max(x, y) + size <= tab.length; size += 2) {
                    squareSlices.add(new SquareSlice(x, y, size));
                }
            }
        }

        return squareSlices.stream().anyMatch(squareSlice -> checkSquare(tab, squareSlice));
    }

    public boolean checkSquare(int[][] tab, SquareSlice squareSlice) {
        for (int x = squareSlice.x; x < squareSlice.x + squareSlice.size - 1; x++) {
            for (int y = squareSlice.y; y < squareSlice.y + squareSlice.size - 1; y++) {
                int value = x + y - squareSlice.x - squareSlice.y;
                if ((value < squareSlice.size - 1 && tab[x][y] != 1) ||
                    (value > squareSlice.size - 1 && tab[x][y] != 0)) {
                    return false;
                }
            }
        }
        return true;
    }
}

class SquareSlice {
    final int x;
    final int y;
    final int size;

    public SquareSlice(int x, int y, int size) {
        this.x = x;
        this.y = y;
        this.size = size;
    }
}